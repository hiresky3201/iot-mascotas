package entry;

import communication.alerts.AlertManager;
import entities.Area;
import entities.Mascota;
import entities.Usuario;
import persistence.Persistence;
import utils.MqttManager;

public class Entry {
    public static void main(String[] args) throws Exception{
        String broker   = "tcp://localhost:1883";
        String clientId = "Servidor#1";

        // -- Crear entidades de inicio
        Persistence persistence = new Persistence();
        Usuario usuario = new Usuario();
        usuario.setId("1ab1143c-92f1-46f7-a15a-9f79df4ce6f6");
        usuario.setName( "Raul Sabag Ballesteros" );
        usuario.setPassword( "contrasena" );
        usuario.setEmail( "hiresky3201@gmail.com" );
        persistence.createEntity( usuario );

        Mascota mascota = new Mascota();
        mascota.setId( "0e066d9a-99c4-432f-bf88-58aa69229e6e" );
        mascota.setName( "Fido" );
        mascota.setUserId( usuario.getId() );
        persistence.createEntity( mascota );

        Area area = new Area();
        area.setUserId( usuario.getId() );
        area.setAreaActual( true );
        area.setLat( 29.0253F );
        area.setLng( -110.947F );
        area.setMetrosMaximosPermitidos( 80 );
        persistence.createEntity( area );

        MqttManager mqttManager = new MqttManager( broker, clientId );
        mqttManager.subscribe( new AlertManager(), "GPS" );
    }
}
