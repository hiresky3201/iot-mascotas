package persistence;

import dev.morphia.query.Query;
import dev.morphia.query.experimental.filters.Filter;
import entities.Entity;
import entities.Mascota;
import entities.Usuario;

import java.util.List;
import java.util.UUID;

public class Persistence {

    /**
     *
     */
    private static MorphiaUtil morphia;

    /**
     * Inicializa en la primera referencia
     */
    static{
        morphia = MorphiaUtil.getInstance();
        morphia.init( "mascotas-iot");
        morphia.getDataStore().getDatabase().getCollection("mascotas").drop();
        morphia.getDataStore().getDatabase().getCollection("usuarios").drop();
        morphia.getDataStore().getDatabase().getCollection("areas").drop();
    }

    public <T extends Entity> boolean createEntity(T entity){
        if( entity.getId() == null )
            entity.setId( UUID.randomUUID().toString() );

        return morphia.saveEntity( entity );
    }

    /**
     * Regresa una entidad de la base de datos
     *
     * @param id La entidad con el ID a buscar.
     * @param tClass El tipo de clase a regresar
     * @return La mascota encontrado, si no se encuentra retornara nulo.
     */
    public <T extends Entity> T getEntityById(String id, Class<T> tClass){
        List<T> mascotas = morphia.getEntities( tClass );

        return mascotas.stream().filter(a -> a.getId().equals( id ) ).findFirst().orElse(null);
    }

    public <T extends Entity> List<T> executeQuery( Class<T> tClass, Filter...filters ){
        return morphia.getDataStore().find( tClass ).filter( filters ).iterator().toList();
    }
}
