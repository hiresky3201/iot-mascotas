package persistence;

import com.mongodb.client.MongoClients;
import dev.morphia.Datastore;
import dev.morphia.Morphia;
import dev.morphia.query.Query;
import entities.Entity;

import java.util.List;

public class MorphiaUtil {
    /**
     * Nuestra instancia de este singleton.
     */
    private static final MorphiaUtil ourInstance = new MorphiaUtil();

    /**
     * Regresa una instancia de MorphiaUtil para acceder a Morphia API.
     * @return
     */
    public static MorphiaUtil getInstance() {
        return ourInstance;
    }

    /**
     * El URL que implica la conexion hacia el servidor.
     */
    private static String mongoURL = "mongodb+srv://mascotasIot:QDMd4Liw3drLyIUW@cluster0.7zs9j.mongodb.net/mascotas-iot?retryWrites=true&w=majority";

    /**
     * Datastore que se usa para comunicarse con mongoDB
     */
    private static Datastore dataStore;

    /**
     * Constructor vacio por default
     */
    private MorphiaUtil() {}

    /**
     * Inicializa Morphia con datos solicitados.
     *
     * @param database
     */
    public void init(String database){
        dataStore = Morphia.createDatastore( MongoClients.create( mongoURL ), database );
        dataStore.getMapper().mapPackage( "entities" );
        dataStore.ensureIndexes();
    }

    /**
     * Guarda una entidad en la base de datos. Si el objeto no es de tipo Entity,
     * no se realizara nada.
     *
     * @param o
     * @return
     */
    public boolean saveEntity(Object o){
        //Checamos que la anotacion Entity este dentro de esta clase..
        if(o.getClass().isAnnotationPresent( dev.morphia.annotations.Entity.class )){
            dataStore.save(o);
            return true;
        }

        //Si no, no hacemos nada.
        return false;
    }

    /**
     * Regresa una entidad dentro de la base de datos.
     *
     * @param <T> El tipo de Entidad.
     * @return La entidad si existe, en otro caso nulo.
     */
    public <T extends Entity> List<T> getEntities(Class<T> tClass){
        Query<T> query = dataStore.createQuery( tClass );
        return query.iterator().toList();
    }

    public Datastore getDataStore(){
        return dataStore;
    }
}
