package entities;

import dev.morphia.annotations.Id;

public class Entity {
    @Id
    private String id;

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }
}
