package entities;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;

@Entity("area")
public class Area extends entities.Entity {
    private String userId;
    private float lat;
    private float lng;
    private boolean areaActual;
    private float metrosMaximosPermitidos;

    public float getLat() { return lat; }

    public void setLat(float lat) { this.lat = lat; }

    public float getLng() { return lng; }

    public void setLng(float lng) { this.lng = lng; }

    public String getUserId() { return userId; }

    public void setUserId(String userId) { this.userId = userId; }

    public boolean isAreaActual() { return areaActual; }

    public void setAreaActual(boolean areaActual) { this.areaActual = areaActual; }

    public float getMetrosMaximosPermitidos() { return metrosMaximosPermitidos; }

    public void setMetrosMaximosPermitidos(float metrosMaximosPermitidos) { this.metrosMaximosPermitidos = metrosMaximosPermitidos; }
}
