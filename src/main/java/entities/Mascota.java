package entities;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;

@Entity("mascotas")
public class Mascota extends entities.Entity {
    private String name;
    private String userId;
    private float lat;
    private float lng;

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getUserId() { return userId; }

    public void setUserId(String userId) { this.userId = userId; }

    public float getLat() { return lat; }

    public void setLat(float lat) { this.lat = lat; }

    public float getLng() { return lng; }

    public void setLng(float lng) { this.lng = lng; }
}
