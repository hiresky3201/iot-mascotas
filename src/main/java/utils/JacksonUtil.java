package utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;

public class JacksonUtil {
    private static ObjectMapper OBJECT_MAPPER;

    public static ObjectMapper getObjectMapper(){
        if( OBJECT_MAPPER != null ) return OBJECT_MAPPER;
        ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        OBJECT_MAPPER = objectMapper;
        return OBJECT_MAPPER;
    }

    public static <T> T tryReadValue( String input , Class<T> tClass ){
        try {
            return getObjectMapper().readValue( input , tClass );
        } catch (IOException e) {
            return null;
        }
    }

    public static <T> T readValue( String input , Class<T> tClass ){
        try {
            return getObjectMapper().readValue( input , tClass );
        } catch (IOException e) {
            throw new RuntimeException("Unable to parse input -> tClass",e);
        }
    }

    public static String writeValueAsString( Object object ){
        try {
            return getObjectMapper().writeValueAsString( object );
        } catch (IOException e) {
            throw new RuntimeException("Unable to serialize object -> string",e);
        }
    }

}
