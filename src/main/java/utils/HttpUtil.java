package utils;

import java.util.HashMap;

public class HttpUtil {
    public static HashMap<String,String> queryToMap(String query){
        HashMap<String,String > r = new HashMap<>();
        if( query == null || !query.contains("=") ) return r;
        query = query.replaceAll("/\\?","");
        String[] fields = query.split("&");
        for (String field : fields ) {
            String[] pair = field.split("=");
            r.put( pair[0] , pair[1] );
        }
        return r;
    }
}
