package tests;

import communication.messages.GPSMessage;
import communication.messages.MqttMessage;
import utils.JacksonUtil;
import utils.MqttManager;

import java.util.Random;

public class SimuladorMascota {
    private static final String mascotaID = "0e066d9a-99c4-432f-bf88-58aa69229e6e";

    public static void main(String[] args) throws Exception {
        String broker   = "tcp://localhost:1883";
        String clientId = "Sensor-GPS-ESP32";

        MqttManager mqttManager = new MqttManager( broker, clientId );
        //createPublishingThread( mqttManager );
        publishAndExit( mqttManager );
    }

    private static void publishAndExit( MqttManager mqttManager ){
        GPSMessage gpsMessage = new GPSMessage();
        gpsMessage.setLat( 0 );
        gpsMessage.setLng( 0 );
        gpsMessage.setMascotaId( mascotaID );

        MqttMessage message = new MqttMessage();
        message.setData( JacksonUtil.writeValueAsString( gpsMessage ) );

        mqttManager.publish( JacksonUtil.writeValueAsString( message ).getBytes(), "GPS" );
        System.out.println("Mensaje enviado");
    }

    private static void createPublishingThread( MqttManager mqttManager ){
        new Thread(() -> {
            float lat = 29.0253F;
            float lng = -110.947F;
            Random random = new Random();

            try{
                while( true ){
                    GPSMessage gpsMessage = new GPSMessage();
                    gpsMessage.setLat( lat );
                    gpsMessage.setLng( lng );
                    gpsMessage.setMascotaId( mascotaID );

                    MqttMessage message = new MqttMessage();
                    message.setData( JacksonUtil.writeValueAsString( gpsMessage ) );

                    lat += random.nextFloat() * 0.1f;
                    lng += random.nextFloat() * 0.1f;

                    mqttManager.publish( JacksonUtil.writeValueAsString( message ).getBytes(), "GPS" );
                    System.out.println("Mensaje enviado");

                    Thread.sleep( 3000 );
                }
            }catch ( Exception e ){
                e.printStackTrace();
            }
        }).start();
    }
}
