package communication.mqtt;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.util.Arrays;

public class MqttManager {
    private final MqttClient client;
    private static final int qos = 2;
    private final MemoryPersistence persistence = new MemoryPersistence();

    public MqttManager(String broker, String clientId) throws Exception{
        client = new MqttClient(broker, clientId, persistence);
        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setCleanSession(true);

        System.out.printf("Connectando al servicio: %s%n", broker );
        client.connect(connOpts);
        System.out.println("Conexion establecida correctamente");
    }

    public void publish( byte[] messageContent, String...topics ){
        MqttMessage message = new MqttMessage( messageContent );
        message.setQos( qos );

        Arrays.stream( topics ).forEach( topic -> {
            try {
                client.publish( topic, message  );
            } catch (MqttException e) {
                e.printStackTrace();
            }
        });
    }

    public void subscribe(IMqttMessageListener listener, String...topics){
        Arrays.stream( topics ).forEach( topic -> {
            try {
                client.subscribe( topic, listener );
            } catch (MqttException e) {
                e.printStackTrace();
            }
        });
    }

    public MemoryPersistence getPersistence(){ return persistence; }
}
