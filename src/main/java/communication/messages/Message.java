package communication.messages;

import java.util.UUID;

public class Message {
    private final String id = UUID.randomUUID().toString();
    private String data;
    private boolean ensureMessage;

    public Message(){}
    public Message( String data, String type, boolean ensureMessage ){
        this.data = data;
        this.ensureMessage = ensureMessage;
    }

    public String getId() { return id; }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean isEnsureMessage() { return ensureMessage; }

    public void setEnsureMessage(boolean ensureMessage) { this.ensureMessage = ensureMessage; }

    @Override
    public String toString() {
        return "Message{" +
                "data='" + data + '\'' +
                ", ensureMessage=" + ensureMessage +
                '}';
    }
}
