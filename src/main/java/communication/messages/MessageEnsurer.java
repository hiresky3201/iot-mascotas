package communication.messages;

import communication.websocket.WebSocketMascotas;
import org.apache.commons.lang3.tuple.Pair;
import utils.JacksonUtil;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MessageEnsurer {
    private final WebSocketMascotas webSocketServer;
    private final ConcurrentHashMap<String, Pair<Message, LocalDateTime>> pendingMessages = new ConcurrentHashMap<>();

    public MessageEnsurer(WebSocketMascotas webSocketServer){
        this.webSocketServer = webSocketServer;

        // -- Crear un scheduler que se encarga de verificar si han llegado mensajes
        ScheduledExecutorService messageMonitor = Executors.newScheduledThreadPool(1);
        messageMonitor.scheduleAtFixedRate( this::verifyPendingMessages, 0, 30, TimeUnit.SECONDS );
    }

    public void ensureMessage(String userId, Message message){
        pendingMessages.put( userId, Pair.of( message, LocalDateTime.now().plus( 30, ChronoUnit.SECONDS ) ) );
    }

    public void acknowledge(Acknowledge acknowledge){
        this.pendingMessages.forEach((key, p) -> {
            if( !p.getLeft().getId().equalsIgnoreCase(acknowledge.getMessageId()) ) return;
            this.pendingMessages.remove( key );
        });
    }

    private void verifyPendingMessages(){
        pendingMessages.forEach((key, p) -> {
            // -- Si ya paso 30 segundos despues de que se mando un mensaje, volver a mandarlo
            if (p.getRight().isAfter(LocalDateTime.now())) return;

            // -- Mandar el mensaje, y volver a poner un timestamp para volver a verificar si se necesita mandare le mensaje otra vez
            webSocketServer.getConnectedUsers().get(key).send(JacksonUtil.writeValueAsString(p.getLeft()));
            pendingMessages.replace(p.getLeft().getId(), Pair.of(p.getLeft(), LocalDateTime.now().plus(30, ChronoUnit.SECONDS)));
        });
    }
}
