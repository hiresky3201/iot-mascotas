package communication.messages;

public class Acknowledge {
    private String messageId;

    public String getMessageId() { return messageId; }

    public void setMessageId(String messageId) { this.messageId = messageId; }
}
