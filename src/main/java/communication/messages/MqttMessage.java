package communication.messages;

public class MqttMessage {
    private String data;

    public MqttMessage(){}
    public MqttMessage( String data ){ this.data = data; }

    public String getData() { return data; }

    public void setData(String data) {
        this.data = data;
    }
}
