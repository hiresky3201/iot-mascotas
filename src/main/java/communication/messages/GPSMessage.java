package communication.messages;

public class GPSMessage extends MqttMessage{
    private String mascotaId;
    private float lat;
    private float lng;

    public GPSMessage(){}

    public String getMascotaId() { return mascotaId; }

    public void setMascotaId(String mascotaId) { this.mascotaId = mascotaId; }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }
}
