package communication.alerts;

import communication.messages.MqttMessage;

public abstract class Alert {
    public abstract String[] topics();
    public abstract void verifyMessage(String topic, MqttMessage message) throws Exception;
}
