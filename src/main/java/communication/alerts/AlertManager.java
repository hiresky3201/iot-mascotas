package communication.alerts;

import communication.messages.GPSMessage;
import communication.websocket.WebSocketMascotas;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import utils.JacksonUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AlertManager implements IMqttMessageListener {
    private final List<Alert> alertList = new ArrayList<>();
    public static WebSocketMascotas webSocketMascotas;

    public AlertManager(){
        alertList.add( new GPSAlert() );
        webSocketMascotas = new WebSocketMascotas();
        webSocketMascotas.execute();
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage){
        // -- Dependiendo del topico, se parsea un tipo distinto de mensaje
        communication.messages.MqttMessage message = parseMessageForTopic( topic, mqttMessage.getPayload() );

        System.out.printf( "Mensaje recibido - %s - %s%n", topic, message.toString() );

        // -- Solo pasar el mensaje recibido por los mecanismos de alerta subscritos a el topico recibido
        List<Alert> relevantAlerts = alertList.stream().filter( a -> Arrays.asList( a.topics() ).contains( topic ) ).collect( Collectors.toList() );
        relevantAlerts.forEach(a -> {
            try {
                a.verifyMessage( topic, message );
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private communication.messages.MqttMessage parseMessageForTopic(String topic, byte[] payload){
        String content = new String( payload );
        communication.messages.MqttMessage message = JacksonUtil.readValue( content, communication.messages.MqttMessage.class );

        if( topic.equalsIgnoreCase( "GPS" ) ){
            return JacksonUtil.readValue( message.getData(), GPSMessage.class );
        }
        else
            return message;
    }
}
