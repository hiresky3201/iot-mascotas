package communication.alerts;

import communication.messages.GPSMessage;
import communication.messages.Message;
import communication.messages.MqttMessage;
import dev.morphia.query.experimental.filters.Filters;
import entities.Area;
import entities.Mascota;
import entities.Usuario;
import persistence.Persistence;
import utils.GeoUtils;

import java.util.List;

public class GPSAlert extends Alert {
    @Override
    public void verifyMessage(String topic, MqttMessage message) throws Exception {
        GPSMessage gpsMessage = ( GPSMessage ) message;

        // -- El mensaje tiene una ID de la mascota, latitud y longitud
        // ocupamos saber que tan lejos de el area del usuario esta
        Persistence persistence = new Persistence();
        Mascota mascota = persistence.getEntityById( gpsMessage.getMascotaId(), Mascota.class );
        Usuario usuario = persistence.getEntityById( mascota.getUserId(), Usuario.class );
        List<Area> areas = persistence.executeQuery( Area.class, Filters.eq("userId", usuario.getId() ) );
        Area area = areas.stream().filter( Area::isAreaActual ).findFirst().orElseThrow( () -> new Exception( "No se tiene ninguna area registrada" ) );

        // -- Verifica si la "mascota" esta a metros legos del lugar
        float distanceInMts = GeoUtils.haversine( area.getLat(), area.getLng(), gpsMessage.getLat(), gpsMessage.getLng() );
        if( distanceInMts < area.getMetrosMaximosPermitidos() ) return;

        // -- Esta muy lejos la mascota, mandar un mensaje de alerta por websocket
        Message msg = new Message();
        msg.setEnsureMessage( true );
        msg.setData( String.format( "Tu Mascota llamada %s ESTA DEMASIADO LEJOS! Metros de distancia: %s" , mascota.getName(), distanceInMts ));
        AlertManager.webSocketMascotas.sendMessageTo( usuario.getId(), msg );
    }

    @Override
    public String[] topics() {
        return new String[]{ "GPS" };
    }
}
