package communication.websocket;

import communication.messages.Acknowledge;
import communication.messages.Message;
import communication.messages.MessageEnsurer;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import utils.HttpUtil;
import utils.JacksonUtil;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

public class WebSocketMascotas extends WebSocketServer {
    private static WebSocketMascotas instance;
    private static MessageEnsurer messageEnsurer;
    private static final Map<String, WebSocket> connections = new HashMap<>();

    public WebSocketMascotas(){}
    public WebSocketMascotas(InetSocketAddress address){ super( address ); }

    public void execute() {
        messageEnsurer = new MessageEnsurer( this );
        instance = new WebSocketMascotas( new InetSocketAddress( "0.0.0.0", 4041) );
        instance.start();
    }

    public void sendMessageTo( String userId, Message message ){
        System.out.println("MESSAGE SENT: " + message.toString());

        if( !connections.containsKey( userId ) ) return;

        // -- Si el mensaje es MUY importante, agregarlo a un batch que se
        // encargara de verificar si el cliente mando un ACKNOWLEDGE
        //
        // Si este no se recibe, entonces se volver a mandar el mensaje
        if( message.isEnsureMessage() ){
            messageEnsurer.ensureMessage( userId, message );
        }

        connections.get( userId ).send( JacksonUtil.writeValueAsString( message ) );
    }

    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        Map<String, String> queryMap = HttpUtil.queryToMap( clientHandshake.getResourceDescriptor() );

        if( !queryMap.containsKey( "userId" ) ){
            webSocket.send("Es necesario conectarte usando un userId como parametro. EJ: ws://endpoint?userId=ID");
            webSocket.close();
            return;
        }

        String userId = queryMap.get( "userId" );
        System.out.printf("Cliente conectado - %s -> %s%n", instance.getAddress(), userId );
        webSocket.setAttachment( userId );
        connections.put( userId, webSocket );
    }

    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        if( webSocket.getAttachment() == null ) return;

        String uuid = webSocket.getAttachment().toString();
        System.out.printf("Cliente desconectado - %s -> %s%n", instance.getAddress(), uuid );
        connections.remove( uuid );
    }

    public void onMessage(WebSocket webSocket, String s) {
        String uuid = webSocket.getAttachment().toString();
        System.out.printf( "[%s - %s]: %s%n", instance.getAddress(), uuid, s );

        Acknowledge acknowledge = JacksonUtil.tryReadValue( s, Acknowledge.class );

        if( acknowledge != null ) {
            messageEnsurer.acknowledge( acknowledge );
        }

        connections.values().forEach( c -> c.send( s ) );
    }

    public void onError(WebSocket webSocket, Exception e) {
        System.err.printf("Error de conexion - %s%n", instance.getAddress() );
        e.printStackTrace();
    }

    public void onStart() {
        System.out.printf("Websocket Server inicializado - %s%n", instance.getAddress() );
    }

    public Map<String, WebSocket> getConnectedUsers() { return connections; }
}
